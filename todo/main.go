package main

import (
    "context"
    "os"
    "log"
    "fmt"

    "github.com/gin-gonic/gin"
    "go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
    "go.mongodb.org/mongo-driver/mongo/readpref"

    "gitlab.com/MoodyNomad/togo-list/todo/server"
)

func main() {
    envs := map[string]string{
        "MONGO_USERNAME": "",
        "MONGO_PASSWORD": "",
        "MONGO_HOST": "",
        "MONGO_PORT": "",
        "TODO_PORT": "",
    }

    for key := range envs {
        envs[key] = os.Getenv(key)
        if envs[key] == "" {
            log.Fatalf("Environment variable %s is undefined", key)
        }
    }

    MongoURI := fmt.Sprintf("mongodb://%s:%s@%s:%s",
        envs["MONGO_USERNAME"], envs["MONGO_PASSWORD"],
        envs["MONGO_HOST"], envs["MONGO_PORT"])

    client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(MongoURI))
    if err != nil {
        panic(err)
    }
    defer func() {
        if err = client.Disconnect(context.TODO()); err != nil {
            panic(err)
        }
    }()

    if err := client.Ping(context.TODO(), readpref.Primary()); err != nil {
        panic(err)
    }

    router := gin.Default()
    server := server.NewTaskServer(context.TODO(), client)

    router.GET("/task/",    server.GetAllTasks)
    router.GET("/task/:id", server.GetTask)

    router.POST("/task/", server.CreateTask)

    router.DELETE("/task/",    server.DeleteAllTasks)
    router.DELETE("/task/:id", server.DeleteTask)

    router.Run(":" + envs["TODO_PORT"])
}
