package server

import (
    "fmt"
    "context"
    "net/http"

    "github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"

    "gitlab.com/MoodyNomad/togo-list/todo/database"
)

type TaskServer struct {
    tlist *database.TaskList
}

func NewTaskServer(context context.Context, client *mongo.Client) *TaskServer {
    return &TaskServer{ database.NewTaskList(context, client) }
}

func (ts *TaskServer) GetTask(c *gin.Context) {
    id := c.Param("id")
    fmt.Print(id)

    task, err := ts.tlist.GetTask(id)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "message": err.Error(),
        })
        return
    }

    c.JSON(http.StatusOK, task)
    return
}

func (ts *TaskServer) GetAllTasks(c *gin.Context) {
    tasks, err := ts.tlist.GetAllTasks()
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "message": err.Error(),
        })
        return
    }

    c.JSON(http.StatusOK, tasks)
}

func (ts *TaskServer) CreateTask(c *gin.Context) {
    type RequestJSON struct {
        Content string `json:"content"`
    }
    var request RequestJSON

    if err := c.ShouldBind(&request); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "message": err.Error(),
        })
    }

    id, err := ts.tlist.AddTask(request.Content)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "message": err.Error(),
        })
        return
    }

    c.JSON(http.StatusOK, gin.H{
        "id": id,
    })
}

func (ts *TaskServer) DeleteTask(c *gin.Context) {
    id := c.Param("id")

    num, err := ts.tlist.DeleteTask(id)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "message": err.Error(),
        })
        return
    }

    c.JSON(http.StatusOK, gin.H{
        "deleted_count": num,
    })
}

func (ts *TaskServer) DeleteAllTasks(c *gin.Context) {
    num, err := ts.tlist.DeleteAllTasks()
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "message": err.Error(),
        })
    }

    c.JSON(http.StatusOK, gin.H{
        "deleted_count": num,
    })
}
