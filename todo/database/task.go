package database

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Task struct {
    ID      string `bson:"_id,omitempty" json:"id"`
    Content string `bson:"content,omitempty" json:"content"`
}

type TaskList struct {
    context    context.Context
    collection *mongo.Collection
}

func NewTaskList(context context.Context, client *mongo.Client) *TaskList {
    tasksCollection := client.Database("tasksServiceDB").Collection("tasks")
    return &TaskList{ context, tasksCollection }
}

func (tl *TaskList) GetTask(idx string) (Task, error) {
    var task Task

    objId, _ := primitive.ObjectIDFromHex(idx)
    err := tl.collection.
        FindOne(tl.context, bson.M{"_id": objId}).
        Decode(&task)

    if err != nil {
        return Task{}, err
    }
    return task, nil
}

func (tl *TaskList) GetAllTasks() ([]Task, error) {
    cur, err := tl.collection. Find(tl.context, bson.M{})
    if err != nil {
        return nil, err
    }

    var tasks []Task
    err = cur.All(tl.context, &tasks)

    return tasks, nil
}

func (tl *TaskList) AddTask(content string) (string, error) {
    task := Task{
        Content: content,
    }
    res, err := tl.collection.InsertOne(tl.context, task)
    if err != nil {
        return "", err
    }

    idx := res.InsertedID.(primitive.ObjectID)
    return idx.Hex(), nil
}

func (tl *TaskList) DeleteTask(idx string) (int64, error) {
    objId, _ := primitive.ObjectIDFromHex(idx)
    res, err := tl.collection. DeleteOne(tl.context, bson.M{"_id": objId})

    if err != nil {
        return 0, err
    }

    return res.DeletedCount, nil
}

func (tl *TaskList) DeleteAllTasks() (int64, error) {
    res, err := tl.collection. DeleteMany(tl.context, bson.M{})

    if err != nil {
        return 0, err
    }

    return res.DeletedCount, nil
}
