(function() {
    var httpRequest;

    document.getElementById("ajaxButton").onclick = function() {
        var content = document.getElementById("ajaxTextbox").value;
        makeRequest('/add', content);
    };

    function makeRequest(url, content) {
        httpRequest = new XMLHttpRequest();

        if (!httpRequest) {
            alert('Cannot create an XMLHTTP instance');
            return false;
        }

        httpRequest.onreadystatechange = alertContents;
        httpRequest.open('POST', url);
        httpRequest.setRequestHeader('Content-Type', 'application/json');
        httpRequest.send(JSON.stringify({"content": content}));
    }

    function alertContents() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            var tblock = document.getElementById("ajaxTextbox");
            if (httpRequest.status === 200) {
                var ul = document.getElementById("taskList")
                var li = document.createElement("li")
                li.appendChild(document.createTextNode(tblock.value));
                ul.appendChild(li);
            } else {
                alert('There was a problem with the request.');
            }
            tblock.value = "";
        }
    }
})();

