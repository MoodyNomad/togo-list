package main

import (
    "os"
    "fmt"
    "log"
	"net/http"
	"encoding/json"

	"github.com/gin-gonic/gin"
	resty "github.com/go-resty/resty/v2"
)

var TodoUrl string

type Task struct {
    ID      string `json:"id"`
    Content string `json:"content"`
}

func main() {
    envs := map[string]string{
        "TODO_HOST": "",
        "TODO_PORT": "",
        "FRONT_PORT": "",
    }

    for key := range envs {
        envs[key] = os.Getenv(key)
        if envs[key] == "" {
            log.Fatalf("Environment variable %s is undefined", key)
        }
    }

    TodoUrl = fmt.Sprintf("http://%s:%s", envs["TODO_HOST"], envs["TODO_PORT"])

    router := gin.Default()

    router.Static("/static", "public/static")
    router.LoadHTMLGlob("public/templates/*")

    router.GET("/", GetMainPage)
    router.POST("/add", AddTask)

    router.Run(":" + envs["FRONT_PORT"])
}

func GetMainPage(c *gin.Context) {
    client := resty.New()

    resp, _ := client.R().Get(TodoUrl + "/task/")

    var tasks []Task
    json.Unmarshal(resp.Body(), &tasks)

    c.HTML(http.StatusOK, "index.html", gin.H{
        "title": "Hello gin",
        "tasks": tasks,
    })
}

func AddTask(c *gin.Context) {
    client := resty.New()

    var task Task

    if err := c.ShouldBind(&task); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "message": err.Error(),
        })
        return
    }

    type ResponseJSON struct {
        ID string `json:"id"`
    }
    var response ResponseJSON

    _, err := client.R().
        SetBody(task).
        SetResult(&response).
        Post(TodoUrl + "/task/")

    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "message": err.Error(),
        })
        return
    }

    c.JSON(http.StatusOK, response)
}
